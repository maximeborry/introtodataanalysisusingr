# Introduction to data analysis using R

Welcome to this introductory course on how to use R for data analysis. The course consists of five
lectures covering different aspects of data analysis:

  1. Introduction the package suite *tidyverse* and data import
  2. Data clean-up and transformation
  3. Exploratory data analysis
  4. Reducing code duplication
  5. Communicating results

For beginners of R, there is additional crash course to bring everyone up on a similar level:

  0. [Crash course](https://bitbucket.org/alexhbnr/introtodataanalysisusingr/src/master/00-crashcourse/)

For each lecture of this course, you will find a dedicated folder with the two files: a RMarkdown
file containing the example code shown during the lecture; a HTML document that contains the
"knitted" output of the RMarkdown document using the R package *Knitr*.

The course is heavily based on the book **"R for data analysis"** by Wickham and Grolemund but takes
the liberty to re-structure its content and introduce additional concepts and packages. The book is
freely available from http://r4ds.had.co.nz.

For each lecture there will be assignments to practice and strengthen your data analysis skills.

Alex Hübner, 2019
